import React, {Component} from 'react';
import {Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import createHistory from 'history/createBrowserHistory';

import './App.css';
import AppRouter from './containers/routes'
import {store} from './redux-store/store'

const history = createHistory();

class App extends Component {
    render() {
        return <Provider store={store}>
            <Router history={history}>
                <AppRouter history={history}/>
            </Router>
        </Provider>
    }


}

export default App;
