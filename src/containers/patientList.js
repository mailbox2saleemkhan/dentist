import React, {Component} from 'react';
import {connect} from 'react-redux';

import {getUserAppointments} from '../redux-store/actions/appointmentActions';
import {PatientListComponent} from '../components';
import {loadAnimations} from "../utils/functions";

class PatientList extends Component {

    componentDidMount() {
        const {getUserAppointments, userDetails} = this.props;
        userDetails && userDetails._id && userDetails.token && getUserAppointments && getUserAppointments({
            authToken: userDetails.token
        })

        loadAnimations();
    }

    render() {
        const {appointmentList} = this.props;
        return <div>
            <section className="home-slider owl-carousel">
                <div className="slider-item bread-item" style={{backgroundImage: `url('/images/bg_1.jpg')`}}
                     data-stellar-background-ratio="0.5">
                    <div className="overlay"></div>
                    <div className="container" data-scrollax-parent="true">
                        <div className="row slider-text align-items-end">
                            <div className="col-md-7 col-sm-12 ftco-animate mb-5">
                                <p className="breadcrumbs"
                                   data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span
                                    className="mr-2"><a href="#">Home</a></span> <span>Patient List</span></p>
                                <h1 className="mb-3"
                                    data-scrollax=" properties: { translateY: '70%', opacity: .9}">Patient List</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="ftco-section">
                <div className="container">
                    <PatientListComponent
                        appointmentList={appointmentList}
                    />
                </div>
            </section>
        </div>
    }

}

export default connect((state = {}, ownProps = {}) => ({
    userDetails: state.user.userDetails,
    appointmentList: state.appointment.appointmentList
}), {getUserAppointments})(PatientList)
