import React from 'react';
import {connect} from 'react-redux';

import {HeaderComponent} from '../components';
import {logoutUser} from '../redux-store/actions/userActions';


class Header extends React.Component {

    constructor(props) {
        super(props);
        this.onLogoutClick = this.onLogoutClick.bind(this);
    }

    onLogoutClick() {
        const {logoutUser, history, userDetails} = this.props;
        userDetails && userDetails.token && logoutUser && logoutUser({authToken: userDetails.token, history});
    }

    render() {

        const {userDetails} = this.props;

        return <HeaderComponent
            onLogoutClick={this.onLogoutClick}
            userDetails={userDetails}
        />
    }
}


export default connect((state = {}, ownProps = {}) => ({
    userDetails: state.user.userDetails,
}), {logoutUser})(Header)