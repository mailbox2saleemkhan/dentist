import React, {Component} from 'react';
import {connect} from 'react-redux';

import {getDepartments} from '../redux-store/actions/appActions';
import {insertNewAppointment} from '../redux-store/actions/appointmentActions';

class AppointmentBookingForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            emailId: null,
            date: null,
            time: null,
            phoneNumber: null,
            department: null,
        };
        this.onDepartmentChange = this.onDepartmentChange.bind(this);
        this.onInputValChange = this.onInputValChange.bind(this);
        this.onBookNewAppointmentClick = this.onBookNewAppointmentClick.bind(this);
    }

    onInputValChange({keyname, event}) {
        this.setState({[keyname]: event.target.value});

    }

    onBookNewAppointmentClick(e) {

        const {insertNewAppointment} = this.props;
        const {time, name, date, emailId, phoneNumber, department} = this.state;

        if (name == null || emailId == null){
            e.preventDefault();
            alert("Please fill all the appointments details!!");
        }
        else{
            insertNewAppointment && insertNewAppointment({
                data: {
                    time,
                    name,
                    date,
                    email_id: emailId,
                    phone_no: phoneNumber,
                    department
                }
            })
        }
    }

    onDepartmentChange(event) {
        this.setState({
            department: event.target.value
        })
    }

    componentDidMount() {
        const {getDepartments} = this.props;
        getDepartments && getDepartments();
    }

    render() {
        const {departmentList} = this.props;
        const {department, time, name, date, emailId, phoneNumber} = this.state;

        return <section className="ftco-intro">

            <div className="container">
                <div className="row no-gutters">
                    <div className="col-md-3 color-1 p-4">
                        <h3 className="mb-4">Emergency Cases</h3>
                        <p>A small river named Duden flows by their place and supplies</p>
                        <span className="phone-number">+ (123) 456 7890</span>
                    </div>
                    <div className="col-md-3 color-2 p-4">
                        <h3 className="mb-4">Opening Hours</h3>
                        <p className="openinghours d-flex">
                            <span>Monday - Friday</span>
                            <span>8:00 - 19:00</span>
                        </p>
                        <p className="openinghours d-flex">
                            <span>Saturday</span>
                            <span>10:00 - 17:00</span>
                        </p>
                        <p className="openinghours d-flex">
                            <span>Sunday</span>
                            <span>10:00 - 16:00</span>
                        </p>
                    </div>
                    <div className="col-md-6 color-3 p-4">
                        <h3 className="mb-2">Make an Appointment</h3>
                        <form action="#" className="appointment-form">
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="form-group">
                                        <div className="select-wrap">
                                            <div className="icon"><span className="ion-ios-arrow-down"/></div>
                                            <select onChange={this.onDepartmentChange} value={department} name="" id=""
                                                    className="form-control">
                                                <option
                                                    value={"Departments"}>Departments
                                                </option>
                                                {departmentList && departmentList.map((department, departmentIndex) =>
                                                    <option key={`${departmentIndex}_dprt`}
                                                            value={department}>{department}</option>)}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="form-group">
                                        <div className="icon"><span className="icon-user"/></div>
                                        <input onChange={_ => {
                                            this.onInputValChange({keyname: "name", event: _})
                                        }} value={name} type="text" className="form-control" id="appointment_name"
                                               placeholder="Name"/>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="form-group">
                                        <div className="icon"><span className="icon-paper-plane"/></div>
                                        <input onChange={_ => {
                                            this.onInputValChange({keyname: "emailId", event: _})
                                        }} value={emailId} type="text" className="form-control" id="appointment_email"
                                               placeholder="Email"/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="form-group">
                                        <div className="icon"><span className="ion-ios-calendar"/></div>
                                        <input onChange={_ => {
                                            this.onInputValChange({keyname: "data", event: _})
                                        }} value={date} type="text" className="form-control appointment_date"
                                               placeholder="Date"/>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="form-group">
                                        <div className="icon"><span className="ion-ios-clock"/></div>
                                        <input onChange={_ => {
                                            this.onInputValChange({keyname: "time", event: _})
                                        }} value={time} type="text" className="form-control appointment_time"
                                               placeholder="Time"/>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="form-group">
                                        <div className="icon"><span className="icon-phone2"/></div>
                                        <input onChange={_ => {
                                            this.onInputValChange({keyname: "phoneNumber", event: _})
                                        }} value={phoneNumber} type="text" className="form-control" id="phone"
                                               placeholder="Phone"/>
                                    </div>
                                </div>
                            </div>

                            <div onClick={this.onBookNewAppointmentClick} className="form-group">
                                <input type="submit" value="Make an Appointment" className="btn btn-primary"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    }
}

export default connect((state = {}, ownProps = {}) => ({
    departmentList: state.app.departmentList,
}), {getDepartments, insertNewAppointment})(AppointmentBookingForm)
