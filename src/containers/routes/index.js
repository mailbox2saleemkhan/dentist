import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import {
    About,
    Home,
    AdminAppointmentList,
    AdminAppointmentHistory,
    PatientProfileForm,
    PatientList,
    BookAppointment,
    PatientsAppointmentList,
    Contact,
    Services,
    Blog,
    Doctors,
    Report,
    ProtectedHome
} from '../index';
import {PageNotExists} from '../../components';
import PublicRoutes from './publicRoutes'
import ProtectedRoutes from './protectedRoutes'


export default class AppRouter extends Component {
    render() {
        const {history} = this.props;
        return <Switch>
            <PublicRoutes history={history} path={"/"} exact component={Home}/>
            <PublicRoutes history={history} path={"/about"} component={About}/>
            <PublicRoutes history={history} path={"/services"} component={Services}/>
            <PublicRoutes history={history} path={"/doctors"} component={Doctors}/>
            <PublicRoutes history={history} path={"/blog"} component={Blog}/>
            <PublicRoutes history={history} path={"/contact"} component={Contact}/>
            <PublicRoutes history={history} path={"/report"} component={Report}/>
            <ProtectedRoutes history={history} path={"/admin/dashboard"} component={ProtectedHome}/>
            <ProtectedRoutes history={history} path={"/admin/appointments"} component={AdminAppointmentList}/>
            <ProtectedRoutes history={history} path={"/admin/appointmentHistory"} component={AdminAppointmentHistory}/>
            <ProtectedRoutes history={history} path={"/admin/bookAppointment"} component={BookAppointment}/>
            <ProtectedRoutes history={history} path={"/admin/patientList"} component={PatientList}/>
            <ProtectedRoutes history={history} path={"/patient/dashboard/:user_id"} component={ProtectedHome}/>
            <ProtectedRoutes history={history} path={"/patient/profile/:user_id"} component={PatientProfileForm}/>
            <ProtectedRoutes history={history} path={"/patient/appointments/:user_id"} component={PatientsAppointmentList}/>
            <ProtectedRoutes history={history} path={"/patient/bookAppointment/:user_id"} component={BookAppointment}/>
            <Route path={"*"} component={PageNotExists}/>
        </Switch>
    }

}
