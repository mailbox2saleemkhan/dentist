import React, {Component} from 'react';
import {connect} from 'react-redux';

import {ProtectedRoutesDashboard} from '../index';
import {checkIsUserValid} from '../../redux-store/actions/userActions';


class ProtectedRoutes extends Component {
    componentDidMount() {
        const {checkIsUserValid, history} = this.props;
        checkIsUserValid && checkIsUserValid({
            onError: _ => {
                history.push("/");
            }
        });
    }

    render() {
        const {userDetails} = this.props;
        if (userDetails) {
            return <ProtectedRoutesDashboard {...this.props}/>
        }
        return null
    }
}

export default connect((state = {}, ownProps = {}) => ({
    userDetails: state.user.userDetails
}), {checkIsUserValid})(ProtectedRoutes)