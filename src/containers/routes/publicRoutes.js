import React, {Component} from 'react';
import {connect} from 'react-redux';

import {PublicRoutesdashboard} from '../index';
import {checkIsUserValid} from '../../redux-store/actions/userActions';


class PublicRoutes extends Component {

    componentDidMount() {
        const {checkIsUserValid} = this.props;
        checkIsUserValid && checkIsUserValid({});
    }

    render() {

        return <PublicRoutesdashboard  {...this.props}/>
    }
}

export default connect((state = {}, ownProps = {}) => ({}), {checkIsUserValid})(PublicRoutes)