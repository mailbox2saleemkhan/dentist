import React from 'react';
import {loadAnimations} from "../utils/functions";


export default class Doctors extends React.Component {
    componentDidMount(){
        loadAnimations()


    }
    render() {

        return <div>
            <section className="home-slider owl-carousel">
                <div className="slider-item bread-item"  style={{backgroundImage: `url('/images/bg_1.jpg')`}}
                     data-stellar-background-ratio="0.5">
                    <div className="overlay"></div>
                    <div className="container" data-scrollax-parent="true">
                        <div className="row slider-text align-items-end">
                            <div className="col-md-7 col-sm-12 ftco-animate mb-5">
                                <p className="breadcrumbs"
                                   data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span
                                    className="mr-2"><a href="index.html">Home</a></span> <span>Services</span></p>
                                <h1 className="mb-3" data-scrollax=" properties: { translateY: '70%', opacity: .9}">Well
                                    Experienced Doctors</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="ftco-section">
                <div className="container">
                    <div className="row justify-content-center mb-5 pb-5">
                        <div className="col-md-7 text-center heading-section ftco-animate">
                            <h2 className="mb-3">Meet Our Experience Dentist</h2>
                            <p>A small river named Duden flows by their place and supplies it with the necessary
                                regelialia. It is a paradisematic country, in which roasted parts of sentences</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-3 col-md-6 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4"  style={{backgroundImage: `url('/images/person_5.jpg')`}} ></div>
                                <div className="info text-center">
                                    <h3><a href="teacher-single.html">Tom Smith</a></h3>
                                    <span className="position">Dentist</span>
                                    <div className="text">
                                        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                        <ul className="ftco-social">
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-twitter"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-facebook"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4"  style={{backgroundImage: `url('/images/person_6.jpg')`}} ></div>
                                <div className="info text-center">
                                    <h3><a href="teacher-single.html">Mark Wilson</a></h3>
                                    <span className="position">Dentist</span>
                                    <div className="text">
                                        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                        <ul className="ftco-social">
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-twitter"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-facebook"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4"  style={{backgroundImage: `url('/images/person_7.jpg')`}} ></div>
                                <div className="info text-center">
                                    <h3><a href="teacher-single.html">Patrick Jacobson</a></h3>
                                    <span className="position">Dentist</span>
                                    <div className="text">
                                        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                        <ul className="ftco-social">
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-twitter"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-facebook"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4"  style={{backgroundImage: `url('/images/person_8.jpg')`}} ></div>
                                <div className="info text-center">
                                    <h3><a href="teacher-single.html">Ivan Dorchsner</a></h3>
                                    <span className="position">System Analyst</span>
                                    <div className="text">
                                        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                        <ul className="ftco-social">
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-twitter"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-facebook"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4"  style={{backgroundImage: `url('/images/person_1.jpg')`}} ></div>
                                <div className="info text-center">
                                    <h3><a href="teacher-single.html">Tom Smith</a></h3>
                                    <span className="position">Dentist</span>
                                    <div className="text">
                                        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                        <ul className="ftco-social">
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-twitter"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-facebook"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4"  style={{backgroundImage: `url('/images/person_2.jpg')`}} ></div>
                                <div className="info text-center">
                                    <h3><a href="teacher-single.html">Mark Wilson</a></h3>
                                    <span className="position">Dentist</span>
                                    <div className="text">
                                        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                        <ul className="ftco-social">
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-twitter"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-facebook"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4"  style={{backgroundImage: `url('/images/person_3.jpg')`}} ></div>
                                <div className="info text-center">
                                    <h3><a href="teacher-single.html">Patrick Jacobson</a></h3>
                                    <span className="position">Dentist</span>
                                    <div className="text">
                                        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                        <ul className="ftco-social">
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-twitter"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-facebook"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4"  style={{backgroundImage: `url('/images/person_4.jpg')`}} ></div>
                                <div className="info text-center">
                                    <h3><a href="teacher-single.html">Ivan Dorchsner</a></h3>
                                    <span className="position">System Analyst</span>
                                    <div className="text">
                                        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                        <ul className="ftco-social">
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-twitter"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-facebook"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                            <li className="ftco-animate"><a href="#"><span
                                                className="icon-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section className="ftco-section ftco-counter img" id="section-counter"
                     style={{backgroundImage: `url('/images/bg_1.jpg')`}} data-stellar-background-ratio="0.5">
                <div className="container">
                    <div className="row d-flex align-items-center">
                        <div className="col-md-3 aside-stretch py-5">
                            <div className=" heading-section heading-section-white ftco-animate pr-md-4">
                                <h2 className="mb-3">Achievements</h2>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                            </div>
                        </div>
                        <div className="col-md-9 py-5 pl-md-5">
                            <div className="row">
                                <div className="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                                    <div className="block-18">
                                        <div className="text">
                                            <strong className="number" data-number="14">0</strong>
                                            <span>Years of Experience</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                                    <div className="block-18">
                                        <div className="text">
                                            <strong className="number" data-number="4500">0</strong>
                                            <span>Qualified Dentist</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                                    <div className="block-18">
                                        <div className="text">
                                            <strong className="number" data-number="4200">0</strong>
                                            <span>Happy Smiling Customer</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                                    <div className="block-18">
                                        <div className="text">
                                            <strong className="number" data-number="320">0</strong>
                                            <span>Patients Per Year</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="ftco-section">
                <div className="container">
                    <div className="row justify-content-center mb-5 pb-5">
                        <div className="col-md-7 text-center heading-section ftco-animate">
                            <h2 className="mb-3">Our Best Pricing</h2>
                            <p>A small river named Duden flows by their place and supplies it with the necessary
                                regelialia.</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 ftco-animate">
                            <div className="pricing-entry pb-5 text-center">
                                <div>
                                    <h3 className="mb-4">Basic</h3>
                                    <p><span className="price">$24.50</span> <span className="per">/ session</span></p>
                                </div>
                                <ul>
                                    <li>Diagnostic Services</li>
                                    <li>Professional Consultation</li>
                                    <li>Tooth Implants</li>
                                    <li>Surgical Extractions</li>
                                    <li>Teeth Whitening</li>
                                </ul>
                                <p className="button text-center"><a href="#"
                                                                     className="btn btn-primary btn-outline-primary px-4 py-3">Order
                                    now</a></p>
                            </div>
                        </div>
                        <div className="col-md-3 ftco-animate">
                            <div className="pricing-entry pb-5 text-center">
                                <div>
                                    <h3 className="mb-4">Standard</h3>
                                    <p><span className="price">$34.50</span> <span className="per">/ session</span></p>
                                </div>
                                <ul>
                                    <li>Diagnostic Services</li>
                                    <li>Professional Consultation</li>
                                    <li>Tooth Implants</li>
                                    <li>Surgical Extractions</li>
                                    <li>Teeth Whitening</li>
                                </ul>
                                <p className="button text-center"><a href="#"
                                                                     className="btn btn-primary btn-outline-primary px-4 py-3">Order
                                    now</a></p>
                            </div>
                        </div>
                        <div className="col-md-3 ftco-animate">
                            <div className="pricing-entry active pb-5 text-center">
                                <div>
                                    <h3 className="mb-4">Premium</h3>
                                    <p><span className="price">$54.50</span> <span className="per">/ session</span></p>
                                </div>
                                <ul>
                                    <li>Diagnostic Services</li>
                                    <li>Professional Consultation</li>
                                    <li>Tooth Implants</li>
                                    <li>Surgical Extractions</li>
                                    <li>Teeth Whitening</li>
                                </ul>
                                <p className="button text-center"><a href="#"
                                                                     className="btn btn-primary btn-outline-primary px-4 py-3">Order
                                    now</a></p>
                            </div>
                        </div>
                        <div className="col-md-3 ftco-animate">
                            <div className="pricing-entry pb-5 text-center">
                                <div>
                                    <h3 className="mb-4">Platinum</h3>
                                    <p><span className="price">$89.50</span> <span className="per">/ session</span></p>
                                </div>
                                <ul>
                                    <li>Diagnostic Services</li>
                                    <li>Professional Consultation</li>
                                    <li>Tooth Implants</li>
                                    <li>Surgical Extractions</li>
                                    <li>Teeth Whitening</li>
                                </ul>
                                <p className="button text-center"><a href="#"
                                                                     className="btn btn-primary btn-outline-primary px-4 py-3">Order
                                    now</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    }
}
