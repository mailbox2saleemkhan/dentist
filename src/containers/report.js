import React from 'react';
import {loadAnimations} from "../utils/functions";


export default class Contact extends React.Component {
    componentDidMount(){
        loadAnimations()
    }
    render() {

        return <div>
            <section className="home-slider owl-carousel">
                <div className="slider-item bread-item"  style={{backgroundImage: `url('/images/bg_1.jpg')`}}
                     data-stellar-background-ratio="0.5">
                    <div className="overlay"></div>
                    <div className="container" data-scrollax-parent="true">
                        <div className="row slider-text align-items-end">
                            <div className="col-md-7 col-sm-12 ftco-animate mb-5">
                                <p className="breadcrumbs"
                                   data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span
                                    className="mr-2"><a href="">Home</a></span> <span>Report</span></p>
                                <h1 className="mb-3"
                                    data-scrollax=" properties: { translateY: '70%', opacity: .9}">Report</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="ftco-section contact-section ftco-degree-bg">
                <div className="container">

                    <div className="col-12 col-lg-6">
                        <div className="card">
                            <div className="card-header"><i className="fa fa-align-justify"></i> Striped Table</div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table className="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Date registered</th>
                                            <th>Role</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Yiorgos Avraamu</td>
                                            <td>2012/01/01</td>
                                            <td>Member</td>
                                            <td><span className="badge badge-success">Active</span></td>
                                        </tr>
                                        <tr>
                                            <td>Avram Tarasios</td>
                                            <td>2012/02/01</td>
                                            <td>Staff</td>
                                            <td><span className="badge badge-danger">Banned</span></td>
                                        </tr>
                                        <tr>
                                            <td>Quintin Ed</td>
                                            <td>2012/02/01</td>
                                            <td>Admin</td>
                                            <td><span className="badge badge-secondary">Inactive</span></td>
                                        </tr>
                                        <tr>
                                            <td>Enéas Kwadwo</td>
                                            <td>2012/03/01</td>
                                            <td>Member</td>
                                            <td><span className="badge badge-warning">Pending</span></td>
                                        </tr>
                                        <tr>
                                            <td>Agapetus Tadeáš</td>
                                            <td>2012/01/21</td>
                                            <td>Staff</td>
                                            <td><span className="badge badge-success">Active</span></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <nav className="" aria-label="pagination">
                                    <ul className="pagination">
                                        <li className="page-item disabled">
                                            <button className="page-link" aria-label="Previous"><span
                                                aria-hidden="true">Prev</span><span className="sr-only">Previous</span>
                                            </button>
                                        </li>
                                        <li className="page-item active">
                                            <button className="page-link">1</button>
                                        </li>
                                        <li className="page-item">
                                            <button className="page-link">2</button>
                                        </li>
                                        <li className="page-item">
                                            <button className="page-link">3</button>
                                        </li>
                                        <li className="page-item">
                                            <button className="page-link">4</button>
                                        </li>
                                        <li className="page-item">
                                            <button className="page-link" aria-label="Next"><span
                                                aria-hidden="true">Next</span><span className="sr-only">Next</span>
                                            </button>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <div className="col-12 col-sm-6">
                        <div className="card">
                            <div className="card-header"><strong>Company</strong>
                                <small> Form</small>
                            </div>
                            <div className="card-body">
                                <div className="position-relative form-group"><label htmlFor="company"
                                                                                     className="">Company</label><input
                                    id="company" placeholder="Enter your company name" type="text"
                                    className="form-control" /></div>
                                <div className="position-relative form-group"><label htmlFor="vat"
                                                                                     className="">VAT</label><input
                                    id="vat" placeholder="DE1234567890" type="text" className="form-control" /></div>
                                <div className="position-relative form-group"><label htmlFor="street"
                                                                                     className="">Street</label><input
                                    id="street" placeholder="Enter street name" type="text" className="form-control" />
                                </div>
                                <div className="my-0 position-relative row form-group">
                                    <div className="col-8">
                                        <div className="position-relative form-group"><label htmlFor="city"
                                                                                             className="">City</label><input
                                            id="city" placeholder="Enter your city" type="text"
                                            className="form-control" /></div>
                                    </div>
                                    <div className="col-4">
                                        <div className="position-relative form-group"><label htmlFor="postal-code"
                                                                                             className="">Postal
                                            Code</label><input id="postal-code" placeholder="Postal Code" type="text"
                                                               className="form-control" /></div>
                                    </div>
                                </div>
                                <div className="position-relative form-group"><label htmlFor="country"
                                                                                     className="">Country</label><input
                                    id="country" placeholder="Country name" type="text" className="form-control" /></div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

        </div>
    }
}
