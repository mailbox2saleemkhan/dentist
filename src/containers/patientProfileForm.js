import React, {Component} from 'react';
import {connect} from 'react-redux';

import {getDepartments} from '../redux-store/actions/appActions';
import {insertNewAppointment} from '../redux-store/actions/appointmentActions';
import {loadAnimations} from "../utils/functions";

class PatientProfileForm extends Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     name: "",
        //     emailId: "",
        //     date: "",
        //     time: "",
        //     phoneNumber: "",
        //     department: "",
        // };
        // this.onDepartmentChange = this.onDepartmentChange.bind(this);
        // this.onInputValChange = this.onInputValChange.bind(this);
        // this.onBookNewAppointmentClick = this.onBookNewAppointmentClick.bind(this);
    }

    // onInputValChange({keyname, event}) {
    //     this.setState({[keyname]: event.target.value});
    //
    // }
    //
    // onBookNewAppointmentClick() {
    //     const {insertNewAppointment} = this.props;
    //     const {time, name, date, emailId, phoneNumber, department} = this.state;
    //     insertNewAppointment && insertNewAppointment({
    //         data: {
    //             time,
    //             name,
    //             date,
    //             email_id: emailId,
    //             phone_no: phoneNumber,
    //             department
    //         }
    //     })
    // }
    //
    // onDepartmentChange(event) {
    //     this.setState({
    //         department: event.target.value
    //     })
    // }

    componentDidMount() {
        //  const {getDepartments} = this.props;
        //  getDepartments && getDepartments();

        loadAnimations();
    }

    render() {
        // const {departmentList} = this.props;
        // const {department, time, name, date, emailId, phoneNumber} = this.state;

        return <div>
            <section className="home-slider owl-carousel">
                <div className="slider-item bread-item" style={{backgroundImage: `url('/images/bg_1.jpg')`}}
                     data-stellar-background-ratio="0.5">
                    <div className="overlay"></div>
                    <div className="container" data-scrollax-parent="true">
                        <div className="row slider-text align-items-end">
                            <div className="col-md-7 col-sm-12 ftco-animate mb-5">
                                <p className="breadcrumbs"
                                   data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span
                                    className="mr-2"><a href="index.html">Home</a></span> <span>Profile</span></p>
                                <h1 className="mb-3"
                                    data-scrollax=" properties: { translateY: '70%', opacity: .9}">Profile</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="ftco-section">
                <div className="container">

                    <div className="col-12">
                        <div className="card">
                            <div className="card-header"><strong>Profile</strong>
                                <small> Update Profile</small>
                            </div>
                            <div className="card-body">
                                <div className="position-relative form-group">
                                    <label htmlFor="company" className=""><b>User Name</b></label>
                                    <input id="user_name" placeholder="Enter your user name" type="text"
                                           className="form-control"/>
                                </div>
                                <div className="position-relative form-group">
                                    <label htmlFor="eamil" className=""><b>Email</b></label>
                                    <input id="email" placeholder="Enter email" type="text" className="form-control"/>
                                </div>
                                <div className="position-relative form-group">
                                    <label htmlFor="phone" className=""><b>Phone Number</b></label>
                                    <input id="phone_number" placeholder="Enter phone number" type="number" className="form-control"/>
                                </div>
                                <div className="position-relative form-group">
                                    <label htmlFor="phone" className=""><b>Gender</b></label>
                                    <div className="col-form-label">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" id="inline-radio1" type="radio"
                                                   value="option1" name="inline-radios"/>
                                                <label className="form-check-label" htmlFor="inline-radio1">Male</label>
                                        </div>
                                        <div className="form-check form-check-inline mr-1">
                                            <input className="form-check-input" id="inline-radio2" type="radio"
                                                   value="option2" name="inline-radios"/>
                                                <label className="form-check-label" htmlFor="inline-radio2">Female</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="position-relative form-group">
                                    <label htmlFor="address" className=""><b>Address</b></label>
                                    <input id="address" placeholder="Enter address" type="text" className="form-control"/>
                                </div>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" type="submit">
                                    <i className="fa fa-dot-circle-o"></i> Submit
                                </button>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
        </div>
    }
}

export default connect((state = {}, ownProps = {}) => ({
    departmentList: state.app.departmentList,
}), {getDepartments, insertNewAppointment})(PatientProfileForm)
