import React from 'react';
import {loadAnimations} from "../utils/functions";


export default class Contact extends React.Component {
    componentDidMount(){
        loadAnimations()
    }
    render() {

        return <div>
            <section className="home-slider owl-carousel">
                <div className="slider-item bread-item"  style={{backgroundImage: `url('/images/bg_1.jpg')`}}
                     data-stellar-background-ratio="0.5">
                    <div className="overlay"></div>
                    <div className="container" data-scrollax-parent="true">
                        <div className="row slider-text align-items-end">
                            <div className="col-md-7 col-sm-12 ftco-animate mb-5">
                                <p className="breadcrumbs"
                                   data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span
                                    className="mr-2"><a href="index.html">Home</a></span> <span>Blog</span></p>
                                <h1 className="mb-3"
                                    data-scrollax=" properties: { translateY: '70%', opacity: .9}">Contact Us</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="ftco-section contact-section ftco-degree-bg">
                <div className="container">
                    <div className="row d-flex mb-5 contact-info">
                        <div className="col-md-12 mb-4">
                            <h2 className="h4">Contact Information</h2>
                        </div>
                        <div className="w-100"></div>
                        <div className="col-md-3">
                            <span>Clinic:</span>
                            <p>Shop # 17A, Block-A/ Eco Bazaar-1, Eco Village-1, Plot-08, Sector-01, Greater Noida - 201303</p>
                            <span>Home Clinic:</span>
                            <p>Q-405, Stellar Jeevan, GH-03, Sector-01, Greater Noida - 201318</p>
                        </div>
                        <div className="col-md-3">
                            <span>Phone:</span>
                            <p> <a href="tel://9810838463">+91 9810838463 <br></br>+91 9811590343</a></p>
                        </div>
                        <div className="col-md-3">
                            <span>Email:</span>
                            <p> <a href="mailto:info@koolsmiles.in">info@koolsmiles.in</a></p>
                        </div>
                        <div className="col-md-3">
                            <span>Website</span>
                            <p> <a href="www.koolsmiles.in">www.koolsmiles.in</a></p>
                        </div>
                    </div>
                    <div className="row block-9">
                        <div className="col-md-6 pr-md-5">
                            <form action="#">
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Your Name" />
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Your Email" />
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Subject" />
                                </div>
                                <div className="form-group">
                                    <textarea name="" id="" cols="30" rows="7" className="form-control"
                                              placeholder="Message"></textarea>
                                </div>
                                <div className="form-group">
                                    <input type="submit" value="Send Message" className="btn btn-primary py-3 px-5" />
                                </div>
                            </form>

                        </div>

                        <div className="col-md-6" id="map"></div>
                    </div>
                </div>
            </section>

        </div>
    }
}
