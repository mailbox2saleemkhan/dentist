import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {Footer, ProtectedHeader, LoginSignupContainer} from "./index";

export default class ProtectedRoutesDashboard extends Component {
    render() {
        const {component, history, path} = this.props;
        return <div>
            <ProtectedHeader history={history}/>

             <Route path={path} component={component}/>

            <Footer/>

            {/*<div>this is header</div>*/}
            {/*<Route path={path} component={component}/>*/}
            {/*<div>this is header</div>*/}

        </div>
    }
}
