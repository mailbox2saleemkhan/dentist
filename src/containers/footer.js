import React from 'react';


export default class Footer extends React.Component {

    render() {

        return <div>
            <footer className="ftco-footer ftco-bg-dark ftco-section">
                <div className="container">
                    <div className="row mb-5">
                        <div className="col-md-3">
                            <div className="ftco-footer-widget mb-4">
                                <h2 className="ftco-heading-2">Kool Smiles</h2>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and
                                    Consonantia, there
                                    live the blind texts.</p>
                            </div>
                            <ul className="ftco-footer-social list-unstyled float-md-left float-lft ">
                                <li className="ftco-animate"><a href="#"><span className="icon-twitter"></span></a></li>
                                <li className="ftco-animate"><a href="#"><span className="icon-facebook"></span></a>
                                </li>
                                <li className="ftco-animate"><a href="#"><span className="icon-instagram"></span></a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-2">
                            <div className="ftco-footer-widget mb-4 ml-md-5">
                                <h2 className="ftco-heading-2">Quick Links</h2>
                                <ul className="list-unstyled">
                                    <li><a href="#" className="py-2 d-block">About</a></li>
                                    <li><a href="#" className="py-2 d-block">Features</a></li>
                                    <li><a href="#" className="py-2 d-block">Projects</a></li>
                                    <li><a href="#" className="py-2 d-block">Blog</a></li>
                                    <li><a href="#" className="py-2 d-block">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-4 pr-md-4">
                            <div className="ftco-footer-widget mb-4">
                                <h2 className="ftco-heading-2">Recent Blog</h2>
                                <div className="block-21 mb-4 d-flex">
                                    <a className="blog-img mr-4"
                                       style={{backgroundImage: `url('/images/image_1.jpg')`}}></a>
                                    <div className="text">
                                        <h3 className="heading"><a href="#">Even the all-powerful Pointing has no
                                            control about</a></h3>
                                        <div className="meta">
                                            <div><a href="#"><span className="icon-calendar"></span> Sept 15, 2018</a>
                                            </div>
                                            <div><a href="#"><span className="icon-person"></span> Admin</a></div>
                                            <div><a href="#"><span className="icon-chat"></span> 19</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="block-21 mb-4 d-flex">
                                    <a className="blog-img mr-4"
                                       style={{backgroundImage: `url('/images/image_2.jpg')`}}></a>
                                    <div className="text">
                                        <h3 className="heading"><a href="#">Even the all-powerful Pointing has no
                                            control about</a></h3>
                                        <div className="meta">
                                            <div><a href="#"><span className="icon-calendar"></span> Sept 15, 2018</a>
                                            </div>
                                            <div><a href="#"><span className="icon-person"></span> Admin</a></div>
                                            <div><a href="#"><span className="icon-chat"></span> 19</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="ftco-footer-widget mb-4">
                                <h2 className="ftco-heading-2">Clinic</h2>
                                <div className="block-23 mb-3">
                                    <ul>
                                        <li><span className="icon icon-map-marker"></span><span className="text">Shop # 17A, Block-A/ Eco Bazaar-1, Eco Village-1, Plot-08, Sector-01, Greater Noida - 201303</span>
                                        </li>
                                        <li><a href="#"><span className="icon icon-phone"></span><span
                                            className="text">+91 9810838463 <br/>+91 9811590343</span></a></li>
                                        <li><a href="#"><span className="icon icon-envelope"></span><span
                                            className="text">info@koolsmiles.in</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 text-center">

                            <p>
                                Copyright &copy;
                                <script>document.write(new Date().getFullYear());</script>
                                 All rights reserved by KoolSmiles
                                {/*| <i className="icon-heart"*/}
                                {/*aria-hidden="true"></i>  <a*/}
                                {/*href="https://colorlib.com" target="_blank"></a>*/}
                            </p>
                        </div>
                    </div>
                </div>
            </footer>

            <div id="ftco-loader" className="show fullscreen">
                <svg className="circular" width="48px" height="48px">
                    <circle className="path-bg" cx="24" cy="24" r="22" fill="none" strokeWidth="4" stroke="#eeeeee"/>
                    <circle className="path" cx="24" cy="24" r="22" fill="none" strokeWidth="4" strokeMiterlimit="10"
                            stroke="#F96D00"/>
                </svg>
            </div>


        </div>
    }
}
