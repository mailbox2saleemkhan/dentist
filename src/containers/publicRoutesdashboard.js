import React from 'react';
import {Route} from 'react-router-dom';
import {Header, Footer, LoginSignupContainer} from './index';


export default class PublicRoutesdashboard extends React.Component {

    render() {
        const {component, history, path} = this.props;
        return <div>

            <Header history={history}/>

            <Route path={path} component={component}/>

            <LoginSignupContainer history={history}/>

            <Footer/>


        </div>
    }
}
