import React, {Component} from 'react';
import {connect} from 'react-redux';

import {PatientsAppointmentListComponent} from '../components';
import {getUserAppointments} from '../redux-store/actions/appointmentActions';

class PatientAppointmentList extends Component {

    componentDidMount() {
        const {getUserAppointments, userDetails} = this.props;
        userDetails && userDetails._id && userDetails.token && getUserAppointments && getUserAppointments({
            appointmentFilter: {"patient_id._id": userDetails._id},
            authToken: userDetails.token
        })
    }

    render() {
        return <PatientsAppointmentListComponent/>
    }
}

export default connect((state = {}, ownProps = {}) => ({
    userDetails: state.user.userDetails
}), {
    getUserAppointments
})(PatientAppointmentList)