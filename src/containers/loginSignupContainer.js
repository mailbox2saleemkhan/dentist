import React, {Component} from 'react'
import {connect} from 'react-redux'

import {LoginSignupComponent} from '../components'
import {loginUser, signUpUser} from '../redux-store/actions/userActions'

class LoginSignupContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoginCase: true,
            emailId: "",
            password: "",
            errorText: undefined,
        };
        this.onMailIdChange = this.onMailIdChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onSignupClick = this.onSignupClick.bind(this);
        this.onSignupSwitchClick = this.onSignupSwitchClick.bind(this);
        this.onLoginSwitchClick = this.onLoginSwitchClick.bind(this);
        this.onLoginClick = this.onLoginClick.bind(this);
        this.onErrorCase = this.onErrorCase.bind(this);
    }

    onSignupClick(e) {
        e.preventDefault();
        const {signUpUser, history} = this.props;
        const {password, emailId} = this.state;
        if (password && emailId) {
            signUpUser && signUpUser({
                history,
                onError: this.onErrorCase,
                emailId,
                password,
            })
        } else {
            this.onErrorCase({error: "Please fill complete details"})
        }
    }

    onSignupSwitchClick() {
        this.setState({isLoginCase: false, errorText: undefined, password: "", emailId: ""})
    }

    onLoginSwitchClick() {
        this.setState({isLoginCase: true, errorText: undefined, password: "", emailId: ""})
    }

    onErrorCase({error}) {
        this.setState({errorText: error})
    }


    onLoginClick(e) {
        e.preventDefault();
        const {loginUser, history} = this.props;
        const {password, emailId} = this.state;
        if (password && emailId) {
            loginUser && loginUser({
                onError: this.onErrorCase,
                history,
                emailId,
                password,
            })
        } else {
            this.onErrorCase({error: "Please fill complete details"})
        }
    }

    onMailIdChange(e) {
        this.setState({emailId: e.target.value, errorText: undefined})
    }

    onPasswordChange(e) {
        this.setState({password: e.target.value, errorText: undefined})
    }

    render() {
        const {isLoginCase, errorText, emailId, password} = this.state;

        return <LoginSignupComponent
            errorText={errorText}
            onMailIdChange={this.onMailIdChange}
            onPasswordChange={this.onPasswordChange}
            password={password}
            emailId={emailId}
            onLoginSwitchClick={this.onLoginSwitchClick}
            onSignupClick={this.onSignupClick}
            onSignupSwitchClick={this.onSignupSwitchClick}
            onLoginClick={this.onLoginClick}
            isLoginCase={isLoginCase}/>
    }
}

export default connect((state = {}, ownProps = {}) => ({}), {loginUser, signUpUser})(LoginSignupContainer)