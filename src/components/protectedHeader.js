import React from 'react';
import logo from './../logo1.png';
import {NavLink} from 'react-router-dom';


export default class ProtectedHeader extends React.Component {

    render() {
        const {userDetails, onLogoutClick} = this.props;

        return <div>
            <nav className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled" id="">
                <div className="container">
                    <NavLink to={"/"} className="navbar-brand"> <img id={"logo_img_id"} className="logo_img" src={logo}
                                                                     alt="Logo"/></NavLink>

                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                            aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="oi oi-menu"></span> Menu
                    </button>

                    <div className="collapse navbar-collapse" id="ftco-nav">
                        <ul className="navbar-nav ml-auto">


                            {
                                userDetails.role == "patient" ? <>
                                    <li className="nav-item active">
                                        <NavLink to={"/patient/dashboard/abc"} className="nav-link">Home</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to={"/patient/profile/abc"} className="nav-link">My Profile</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to={"/patient/bookAppointment/abc"} className="nav-link">Book Appointment</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to={"/patient/appointmentHistory/abc"} className="nav-link">Appointment History</NavLink>
                                    </li>
                                </> : <>
                                    <li className="nav-item active">
                                        <NavLink to={"/admin/dashboard"} className="nav-link">Home</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to={"/admin/appointments"} className="nav-link">Appointment</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to={"/admin/appointmentHistory"} className="nav-link">History</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to={"/admin/bookAppointment"} className="nav-link">Book Appointment</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to={"/admin/patientList"} className="nav-link">Patients List</NavLink>
                                    </li>
                                </>
                            }

                            {userDetails && <li className="nav-item cta">
                                <a href="#" className="nav-link" data-toggle="modal"
                                   data-target="#modalRequest"><span onClick={onLogoutClick}>Logout</span></a>
                            </li>}
                        </ul>
                    </div>
                </div>
            </nav>

        </div>
    }
}
