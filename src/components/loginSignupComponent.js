import React, {Component} from 'react'

export default class LoginSignupComponent extends Component {

    render() {
        const {isLoginCase, errorText, onPasswordChange, onMailIdChange, emailId, password, onLoginClick, onSignupSwitchClick, onSignupClick, onLoginSwitchClick} = this.props;
        return <div className="modal fade" id="modalRequest" tabIndex="-1" role="dialog"
                    aria-labelledby="modalRequestLabel"
                    aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="modalRequestLabel">{isLoginCase ? 'Login' : "Signup"}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form action="#">
                            <div className="form-group">
                                <input onChange={onMailIdChange} value={emailId} type="text" className="form-control"
                                       id="appointment_name"
                                       placeholder="Email Id"/>
                            </div>
                            <div className="form-group">
                                <input onChange={onPasswordChange} value={password} type="password"
                                       className="form-control" id="appointment_email"
                                       placeholder="Password"/>
                            </div>
                            {/*<div className="row">*/}
                            {/*<div className="col-md-6">*/}
                            {/*<div className="form-group">*/}
                            {/*<input type="text" className="form-control appointment_date"*/}
                            {/*placeholder="Date"/>*/}
                            {/*</div>*/}
                            {/*</div>*/}
                            {/*<div className="col-md-6">*/}
                            {/*<div className="form-group">*/}
                            {/*<input type="text" className="form-control appointment_time"*/}
                            {/*placeholder="Time"/>*/}
                            {/*</div>*/}
                            {/*</div>*/}
                            {/*</div>*/}


                            {/*<div className="form-group">*/}
                            {/*<textarea name="" id="appointment_message" className="form-control" cols="30"*/}
                            {/*rows="10"*/}
                            {/*placeholder="Message"></textarea>*/}
                            {/*</div>*/}
                            {errorText && <div style={{color: "red"}}>{errorText}</div>}
                            <div className="form-group">
                                <input onClick={isLoginCase ? onLoginClick : onSignupClick} type="submit"
                                       value={`${isLoginCase ? "Login" : "Signup"}`}
                                       className="btn btn-primary"/>
                            </div>

                        </form>
                        {isLoginCase && <div onClick={onSignupSwitchClick}>Signup</div>}
                        {!isLoginCase && <div onClick={onLoginSwitchClick}>Login</div>}
                    </div>

                </div>
            </div>
        </div>
    }
}