import React from 'react';
import logo from './../logo1.png';
import {NavLink} from 'react-router-dom';


export default class Header extends React.Component {

    render() {
        const {userDetails, onLogoutClick} = this.props;

        return <div>
            <nav className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
                <div className="container">
                    <NavLink to={"/"} className="navbar-brand"> <img id={"logo_img_id"} className="logo_img" src={logo}
                                                                     alt="Logo"/></NavLink>

                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                            aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="oi oi-menu"></span> Menu
                    </button>

                    <div className="collapse navbar-collapse" id="ftco-nav">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item active">
                                <NavLink to={"/"} className="nav-link">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/about"} className="nav-link">About</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/services"} className="nav-link">Services</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/doctors"} className="nav-link">Doctors</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/blog"} className="nav-link">Blog</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to={"/contact"} className="nav-link">Contact</NavLink>
                            </li>
                            {!userDetails && <li className="nav-item cta">
                                <a href="#" className="nav-link" data-toggle="modal"
                                   data-target="#modalRequest"><span>Login</span></a>
                            </li>}
                            {userDetails && <li className="nav-item cta">
                                <a href="#" className="nav-link" data-toggle="modal"
                                   data-target="#modalRequest"><span onClick={onLogoutClick}>Logout</span></a>
                            </li>}
                        </ul>
                    </div>
                </div>
            </nav>

        </div>
    }
}
