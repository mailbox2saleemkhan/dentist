import React, {Component} from 'react';

export default class PatientList extends Component {
    render() {
        const {appointmentList} = this.props;
        return <div>
            <div className="col-12">
                <div className="card">
                    <div className="card-header"><i className="fa fa-align-justify"></i>Patient List</div>
                    <div className="card-body">
                        <div className="table-responsive">
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Email</th>
                                    <th>Mobile No</th>
                                    <th>Department</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {appointmentList && appointmentList.length > 0 && appointmentList.map((data) =>
                                    <tr>
                                    <td>{data && data.name}</td>
                                    <td>{data && data.date}</td>
                                    <td>{data && data.time}</td>
                                    <td>{data && data.email_id}</td>
                                    <td>{data && data.phone_no}</td>
                                    <td>{data && data.department}</td>
                                    <td><span className="badge badge-success">Active</span></td>
                                    <td><span className="badge badge-success">Button</span></td>
                                    </tr>

                                )}

                                </tbody>
                            </table>
                        </div>
                        {/*<nav className="" aria-label="pagination">*/}
                            {/*<ul className="pagination">*/}
                                {/*<li className="page-item disabled">*/}
                                    {/*<button className="page-link" aria-label="Previous"><span*/}
                                        {/*aria-hidden="true">Prev</span><span className="sr-only">Previous</span>*/}
                                    {/*</button>*/}
                                {/*</li>*/}
                                {/*<li className="page-item active">*/}
                                    {/*<button className="page-link">1</button>*/}
                                {/*</li>*/}
                                {/*<li className="page-item">*/}
                                    {/*<button className="page-link">2</button>*/}
                                {/*</li>*/}
                                {/*<li className="page-item">*/}
                                    {/*<button className="page-link">3</button>*/}
                                {/*</li>*/}
                                {/*<li className="page-item">*/}
                                    {/*<button className="page-link">4</button>*/}
                                {/*</li>*/}
                                {/*<li className="page-item">*/}
                                    {/*<button className="page-link" aria-label="Next"><span*/}
                                        {/*aria-hidden="true">Next</span><span className="sr-only">Next</span>*/}
                                    {/*</button>*/}
                                {/*</li>*/}
                            {/*</ul>*/}
                        {/*</nav>*/}
                    </div>
                </div>
            </div>


        </div>
    }

}
