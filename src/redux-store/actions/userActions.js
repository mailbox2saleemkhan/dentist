import types from './types';
import {setItemInPersistentStore, getItemFromPersistentStore, clearPersistentStore} from '../../utils/persist'

const onLoginSignupSuccess = ({data, history}) => {
    if (data && data[0] && data[0].response && data[0].response.length > 0 && data[0].response[0].role) {
        setItemInPersistentStore("UserDetails", data[0].response[0]);
        setItemInPersistentStore("AuthToken", data[0].response[0].token);
        if (data[0].response[0].role === "patient") {
            return history.push(`/patient/dashboard/${data[0].response[0]._id}`)
        } else {
            return history.push(`/admin/dashboard`)
        }
    }
};
const onLogoutUser = ({authToken, history, next, actions}) => {
    next(actions);
    clearPersistentStore();
    return history.push(`/`);
};
export const loginUser = payload => {
    const {onError, history, password, emailId} = payload;
    return {
        type: types.users.LOGIN_USER,
        fetchConfig: [{
            path: `/login?user=${JSON.stringify({email_id: emailId, password})}`,
        }],
        onSuccess: ({data, store, next, actions}) => {
            // alert(JSON.stringify(data))
            if (data && data[0] && data[0].response && data[0] && data[0].response.length > 0) {
                actions.payload = {...actions.payload, data: data[0].response[0]};
                next(actions)
            }

            onLoginSignupSuccess({data, history})
        },
        onError: ({error, store}) => {
            onError && onError({error})
        }
    }
};
export const checkIsUserValid = payload => {
    const {onError} = payload;
    const userDetails = getItemFromPersistentStore("UserDetails");
    return {
        type: types.users.CHECK_IS_USER_VALID,
        fetchConfig: [{
            path: `/query?query=${JSON.stringify({
                "collection": "Users",
                filter: {"_id": userDetails && userDetails._id}
            })}&token=${getItemFromPersistentStore("AuthToken")}`,
        }],
        onSuccess: ({data, store, next, actions}) => {
            // alert(JSON.stringify(data))
            if (data && data[0] && data[0].response && data[0] && data[0].response.length > 0) {
                delete data[0].response[0].tokens;
                data[0].response[0].token = userDetails.token;
                actions.payload = {...actions.payload, data: data[0].response[0]};
                next(actions)
            }


        },
        onError: ({error, store}) => {
            onError && onError({error})
        }
    }
};
export const signUpUser = payload => {
    const {emailId, history, password, onError} = payload;
    return {
        type: types.users.SIGNUP_USER,
        fetchConfig: [{
            path: `/signup?user=${JSON.stringify({email_id: emailId, password})}`,
        }],
        onSuccess: ({data, store, next, actions}) => {
            // alert(JSON.stringify(data))

            if (data && data[0] && data[0].response && data[0].response.length > 0) {
                actions.payload = {...actions.payload, data: data[0].response[0]};
                next(actions)
            }
            onLoginSignupSuccess({data, history})
        },
        onError: ({error, store}) => {
            onError && onError({error})
        }
    }
};
export const logoutUser = payload => {
    const {authToken, history} = payload;
    return {
        type: types.users.LOGOUT_USER,
        fetchConfig: [{
            path: `/logout?token=${authToken}`,
        }],
        onSuccess: ({data, store, next, actions}) => {
            onLogoutUser({authToken, history, next, actions})
        },
        onError: ({error, store, next, actions}) => {
            onLogoutUser({authToken, history, next, actions})
        }
    }
};
