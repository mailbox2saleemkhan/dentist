export default {
    "users": {
        "LOGIN_USER": "LOGIN_USER",
        "SIGNUP_USER": "SIGNUP_USER",
        "LOGOUT_USER": "LOGOUT_USER",
        "CHECK_IS_USER_VALID": "CHECK_IS_USER_VALID",
    },
    "app": {
        "GET_DEPARTMENTS": "GET_DEPARTMENTS",
        "SWITCH_LOADING_STATUS": "SWITCH_LOADING_STATUS",
    },
    "appointments": {
        "GET_USER_APPOINTMENTS": "GET_USER_APPOINTMENTS",
        "INSERT_NEW_APPOINTMENT": "INSERT_NEW_APPOINTMENT",
    }
}