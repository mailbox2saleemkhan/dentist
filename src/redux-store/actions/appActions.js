import types from './types';

export const switchLoadingStatus = payload => ({type: types.app.SWITCH_LOADING_STATUS, payload});

export const getDepartments = payload => {
    return {
        type: types.app.GET_DEPARTMENTS,
        fetchConfig: [{
            path: `/get_departments`,
        }],
        onSuccess: ({data, store, next, actions}) => {
          actions.payload={...actions.payload,data:data[0].response}
          next(actions)
        },

    }
};