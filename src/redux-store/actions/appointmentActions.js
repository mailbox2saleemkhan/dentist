import types from './types';


export const insertNewAppointment = payload => {
    const {data} = payload;
    return {
        type: types.appointments.INSERT_NEW_APPOINTMENT,
        fetchConfig: [{
            path: `/insert?insert=${JSON.stringify({
                "collection": "Appointments",
                "insert": data
            })}&authenticateUser=false`,
        }],
        onSuccess: ({data, store, next, actions}) => {
            alert("Appointment Booked Successfully !! ")
            // actions.payload = {...actions.payload, data: data[0].response};
            // next(actions)
        },
        onError: ({error}) => {
        }

    }
};

export const getUserAppointments = payload => {
    const {appointmentFilter, authToken} = payload;
    return {
        type: types.appointments.GET_USER_APPOINTMENTS,
        fetchConfig: [{
            path: `/query?query=${JSON.stringify({
                "collection": "Appointments",
                "filter": appointmentFilter
            })}&token=${authToken}`,
        }],
        onSuccess: ({data, store, next, actions}) => {
            if (data && data.length > 0 && data[0].response) {
                actions.payload = {...actions.payload, data: data[0].response}
                next(actions)
            }
        },
        onError: ({error}) => {
        }

    }
};