import reducers from './reducers';
import middlewares from './middlewares';

module.exports = {
    middlewares,
    reducers
};