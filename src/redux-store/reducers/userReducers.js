import types from '../actions/types';

const initialState = {isAppLoading: false, userDetails: undefined};

const UserReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case types.users.SIGNUP_USER: {
            const {data} = actions.payload;
            return {...state, userDetails: data};
        }
        case types.users.LOGIN_USER: {
            const {data} = actions.payload;
            return {...state, userDetails: data};
        }
        case types.users.CHECK_IS_USER_VALID: {
            const {data} = actions.payload;
            return {...state, userDetails: data};
        }
        case types.users.LOGOUT_USER: {
            return {...state, userDetails: undefined};
        }
        default:
            return state
    }
};
export default UserReducer