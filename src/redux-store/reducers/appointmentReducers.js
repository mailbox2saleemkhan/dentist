import types from '../actions/types';

const initialState = {appointmentList: undefined};

const appointmentReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case types.appointments.GET_USER_APPOINTMENTS:
            const {data} = actions.payload;
            return {...state, appointmentList: data}
        default:
            return state
    }
};
export default appointmentReducer