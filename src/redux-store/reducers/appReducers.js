import types from '../actions/types';

const initialState = {isAppLoading: false};

const AppReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case types.app.SWITCH_LOADING_STATUS:
            const {status} = actions.payload;
            return {...state, isAppLoading: status};
        case types.app.GET_DEPARTMENTS:
            const {data} = actions.payload;
            return {...state, departmentList: data};
        default:
            return state
    }
};
export default AppReducer