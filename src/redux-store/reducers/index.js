import {combineReducers} from 'redux';
import appReducers from './appReducers'
import appointmentReducers from './appointmentReducers'
import userReducers from './userReducers'

const allReducers = combineReducers({
    user: userReducers,
    app: appReducers,
    appointment: appointmentReducers,
});

export default allReducers