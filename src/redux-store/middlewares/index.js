import {switchLoadingStatus} from '../actions/appActions';
import {getDataFromServer} from '../../utils/functions';


const middlewares = ((store = {}) => {
    return function (next) {
        return function (action) {
            if (action && action.fetchConfig && Array.isArray(action.fetchConfig)) {

                const {onError, onSuccess} = action;
                const serverCallsArray = [];
                store.dispatch(switchLoadingStatus({status: true}));

                if (action.fetchConfig) {
                    action.fetchConfig.forEach(config => {
                        const {path, body, method = "GET", headers = {}} = config;
                        serverCallsArray.push(getDataFromServer({
                            path,
                            body,
                            method,
                            headers: {...headers},
                        }))
                    });
                }


                return Promise.all(serverCallsArray).then(responses => {
                    store.dispatch(switchLoadingStatus({status: false}));
                    onSuccess && onSuccess({data: responses, store, next, actions: action});
                }).catch(error => {
                    store.dispatch(switchLoadingStatus({status: false}));
                    onError && onError({error: error.body.message, store, next, actions: action});
                });

            } else {
                next(action);
            }
        }
    }
});
export default [middlewares];
