
var google;

// function init() {

    var markers = {
        "title": 'Kool Smiles',
        "lat": '28.470840',
        "lng": '77.509790'
    }

    // var map;

function setMap(){
    var mapOptions = {
        center: new google.maps.LatLng(markers.lat, markers.lng),
        zoom: 9,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var myLatlng = new google.maps.LatLng(markers.lat, markers.lng);

    var icon = {
        // url: "../img/ThePlasticExchangeMap.png",
        scaledSize: new google.maps.Size(30, 30)
    };

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        animation: google.maps.Animation.DROP,
        title: markers.title,
        //  icon: icon
    });
    // (function (marker, markers) {
    //     google.maps.event.addListener(marker, "click", function (e) {
    //         SetZoom(map, marker);
    //         CreateCircle(map, marker);
    //         google.maps.event.clearListeners(marker, "click");
    //     });
    // })(marker, markers);
}

    window.onload = function () {
        setMap();
    };
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    // var myLatlng = new google.maps.LatLng(40.71751, -73.990922);
    // var myLatlng = new google.maps.LatLng(40.69847032728747, -73.9514422416687);
    // // 39.399872
    // // -8.224454
    //
    // var mapOptions = {
    //     // How zoomed in you want the map to start at (always required)
    //     zoom: 7,
    //
    //     // The latitude and longitude to center the map (always required)
    //     center: myLatlng,
    //
    //     // How you would like to style the map.
    //     scrollwheel: false,
    //     styles: [
    //         {
    //             "featureType": "administrative.country",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "visibility": "simplified"
    //                 },
    //                 {
    //                     "hue": "#ff0000"
    //                 }
    //             ]
    //         }
    //     ]
    // };
    //
    //
    //
    // // Get the HTML DOM element that will contain your map
    // // We are using a div with id="map" seen below in the <body>
    // var mapElement = document.getElementById('map');
    //
    // // Create the Google Map using out element and options defined above
    // var map = new google.maps.Map(mapElement, mapOptions);
    //
    // var addresses = ['New York'];
    //
    // for (var x = 0; x < addresses.length; x++) {
    //     $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+addresses[x]+'&sensor=false', null, function (data) {
    //         var p = data.results[0].geometry.location
    //         var latlng = new google.maps.LatLng(p.lat, p.lng);
    //         new google.maps.Marker({
    //             position: latlng,
    //             map: map,
    //             icon: 'images/loc.png'
    //         });
    //
    //     });
    // }
//
// }
// google.maps.event.addDomListener(window, 'load', init);
