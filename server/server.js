import express from 'express';
import config from './src/HTTP';


const app = express();

config(app);

const server = app.listen(process.env.PORT, _ => {
    console.log(`Dentist App Server is running on ${server.address().port} port`)
});