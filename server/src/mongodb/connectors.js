import mongo from 'mongodb';
import q from 'q';
import i18n from 'i18n';
import functions from '../functions/index';
import {mongoInsert, mongoUpdate, mongoQuery, mongoRemove} from './curd_operations';
import {dbActions, dbList, mongoUrl} from '../constants';
import {
    consoleLog,
    resolveQueryFilters,
    convertFieldValsBasedOnRespectiveTypes,
    checkForMandatoryFieldsAndValidFkValues
} from '../Utility';

const MongoClient = mongo.MongoClient;

export const connectToDB = _ => {
    const d = q.defer();
    MongoClient.connect(mongoUrl, (err, conndb) => {
        if (err) {
            d.reject(err);
            return;
        }
        d.resolve(conndb)
    });
    return d.promise;
};


export const getData = args => {

    consoleLog("getData args >> >> ", args);

    return connectToDB().then(db => {

        const options = {};

        if (args.fields) {
            options["fields"] = args.fields;
        }

        return mongoQuery(db, args.collection, resolveQueryFilters(args), options, (args.skip ? args.skip : 0), (args.limit ? args.limit : 1000), (args.sort ? args.sort : {}));
    }).then(recordData => {
        consoleLog("server response on get data >> >> ", recordData);
        return recordData
    })

};

export const updateData = args => {

    consoleLog("updateData args >> >> ", args);

    let connectedDB = undefined;
    const events = args.hasOwnProperty("events") ? args.events : true;
    const collectionName = args.collection;
    let oldRecordData = undefined;


    if (args && args.filter && args.filter._id && args.update) {
        const collFilter = resolveQueryFilters(args);
        return connectToDB().then(db => {
            connectedDB = db;

            const record_id = mongo.ObjectID(collFilter._id);

            return mongoQuery(db, collectionName, {_id: record_id}, {}, 0, 1000, {});
        }).then(recordData => {
            consoleLog("old recordData while updating >> >> ", recordData);
            oldRecordData = recordData;
            if (oldRecordData && oldRecordData.length > 0) {
                if (events && functions && Object.keys(functions).length > 0 && functions.hasOwnProperty(collectionName) && functions[collectionName].beforeJob) {
                    return functions[collectionName].beforeJob(connectedDB, args.update, {
                        operation: dbActions.UPDATE,
                        oldDocument: oldRecordData[0]
                    });
                }
            } else {
                throw new Error(("noRecordExistsErrorWhenNoDataFoundInUpdateCase"));
            }

        }).then(_ => {

            return checkForMandatoryFieldsAndValidFkValues(connectedDB, {
                operation: dbActions.UPDATE,
                oldDocument: oldRecordData[0],
            }, args.update, collectionName)

        }).then(_ => {

            if (args.update && Object.keys(args.update).length > 0) {
                for (const updateJsonKey in args.update) {
                    // updateJsonKey could be $set,$push.$pull etc
                    if (args.update.hasOwnProperty(updateJsonKey)) {
                        args.update[updateJsonKey] = convertFieldValsBasedOnRespectiveTypes(args.update[updateJsonKey], dbList[collectionName]);
                    }
                }
            }

            return mongoUpdate(connectedDB, collectionName, collFilter, args.update)

        }).then(finalRes => {
            consoleLog("server response on update data >> >> ", finalRes);
            return finalRes
        })

    } else {
        const d = q.defer();
        d.reject(new Error(("idOrUpdateJsonNotFoundDuringUpdate")));
        return d.promise
    }
};

export const insertData = args => {

    consoleLog("insertData args >> >> ", args);

    const events = args.hasOwnProperty("events") ? args.events : true;
    const collectionName = args.collection;
    let connectedDB = undefined;

    args.insert = convertFieldValsBasedOnRespectiveTypes(args.insert, dbList[collectionName]);

    if (args && args.insert && Object.keys(args.insert).length > 0) {

        return connectToDB().then(db => {
            connectedDB = db;

            if (events && functions && Object.keys(functions).length > 0 && functions.hasOwnProperty(collectionName) && functions[collectionName].beforeJob) {
                return functions[collectionName].beforeJob(db, args.insert, {operation: dbActions.INSERT});
            }

        }).then(_ => {

            return checkForMandatoryFieldsAndValidFkValues(connectedDB, {operation: dbActions.INSERT}, args.insert, collectionName)

        }).then(finalData => {

            return mongoInsert(connectedDB, collectionName, (finalData ? finalData : args.insert));

        }).then(response => {
            consoleLog("server response on insert data >> >> ", response);
            return response
        })
    } else {
        const d = q.defer();
        d.reject(new Error(("insertJsonNotFoundDuringInsert")));
        return d.promise
    }
};


export const removeData = args => {

    consoleLog("removeData args >> >> ", args);

    const collectionName = args.collection;
    const collFilter = resolveQueryFilters(args);


    const events = args.hasOwnProperty("events") ? args.events : true;
    let connectedDB = undefined;
    return connectToDB().then(db => {
        connectedDB = db;

        if (events && functions && Object.keys(functions).length > 0 && functions.hasOwnProperty(collectionName) && functions[collectionName].beforeJob) {
            return functions[collectionName].beforeJob(db, collFilter, {operation: dbActions.DELETE});
        }
    }).then(_ => {
        return mongoRemove(connectedDB, collectionName, collFilter);
    }).then(response => {
        consoleLog("server response on delete data >> >> ", response);
        return response;
    })
};

