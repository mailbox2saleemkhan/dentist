import q from 'q';
import {dbList, fieldTypes} from '../constants';
import {consoleLog, iterator} from '../Utility'

export const mongoInsert = function (db, collectionName, data) {
    const d = q.defer();
    db.collection(collectionName).insert(data, function (err, result) {
        d.resolve(result)
    });
    return d.promise;
};

export const mongoUpdate = function (db, collectionName, filter, finalUpdates) {
    const d = q.defer();
    db.collection(collectionName).update(filter, finalUpdates, function (err, result) {
        d.resolve(result)
    });
    return d.promise;
};

const getDottedFieldValue = ({db, result, options, DBDefination}) => {
    const d = q.defer();
    // handling to get dotted fields from fk collection
    const fieldsArrayForSubquery = [];
    const fieldsListForObjectFields = [];
    // consoleLog("........................options >> >> ", options);
    // consoleLog("........................DBDefination >> >> ", DBDefination);

    if (options && options.fields && Object.keys(options.fields).length > 0) {

        if (DBDefination && DBDefination.fields && DBDefination.fields.length > 0) {
            for (const count in DBDefination.fields) {
                if (DBDefination.fields[count].type && DBDefination.fields.hasOwnProperty(count) && options.fields.hasOwnProperty(DBDefination.fields[count].name)) {
                    if (DBDefination.fields[count].type === fieldTypes.fk) {
                        fieldsArrayForSubquery.push(DBDefination.fields[count]);
                    } else if (DBDefination.fields[count].type === fieldTypes.Object) {
                        fieldsListForObjectFields.push(DBDefination.fields[count]);
                    }
                }
            }
        }
    }
    // consoleLog("fieldsListForObjectFields >> >> ", fieldsListForObjectFields);
    // consoleLog("fieldsArrayForSubquery >> >> ", fieldsArrayForSubquery);
    // consoleLog("result >> >> ", result);
    if (result && result.length > 0) {

        iterator(result, function (resIndex, eachResult) {
            // consoleLog("eachResult >> >> ", eachResult);

            return iterator(fieldsArrayForSubquery, function (index, field) {
                // consoleLog("field >> >> ", field);

                if (eachResult.hasOwnProperty(field.name) && eachResult[field.name]._id) {
                    let subQueryOptions = {};
                    if (typeof options.fields[field.name] === "object" && Object.keys(options.fields[field.name]).length > 0) {
                        subQueryOptions = {fields: options.fields[field.name]};
                    }
                    return mongoQuery(db, field.referred_collection, {_id: eachResult[field.name]._id}, subQueryOptions).then(function (fkResult) {
                        // consoleLog(".............field val >> >> ", options.fields[field.name]);
                        result[resIndex][field.name] = fkResult[0];
                    }).catch(function (err) {
                        consoleLog("err " + err)
                    })
                }
            }).then(_ => {
                // consoleLog("first then ... " + JSON.stringify(result));
                return iterator(fieldsListForObjectFields, function (objFieldIndex, objFieldVal) {
                    // consoleLog("objFieldVal >> >> ", objFieldVal);

                    if (eachResult.hasOwnProperty(objFieldVal.name)) {

                        return getDottedFieldValue({
                            db,
                            result: Array.isArray(eachResult[objFieldVal.name]) ? eachResult[objFieldVal.name] : [eachResult[objFieldVal.name]],
                            options: {fields: options.fields[objFieldVal.name]},
                            DBDefination: objFieldVal
                        });


                    }
                })
            })

        }).then(_ => {
            consoleLog("result result >> >> ", result);
            d.resolve(result);
        })


    } else {
        d.resolve(result);
    }
    return d.promise;


};
export const mongoQuery = function (db, collectionName, collFilter, options, skip, limit, sort) {

    const d = q.defer();
    const queryLimit = limit || 1000;
    const querySkipCount = skip || 0;

    const findOptions = {};

    if (!sort) {
        sort = {};
    }

    const getDataData = (filterJsonObject, keyName, keyVal) => {
        // consoleLog('keyName >> ' + JSON.stringify(keyName))
        // consoleLog('keyVal >> ' + JSON.stringify(keyVal))
        if (keyName.indexOf('.') === -1) {
            filterJsonObject[keyName] = keyVal;
        } else {
            if (!filterJsonObject.hasOwnProperty(keyName.split('.')[0])) {
                filterJsonObject[keyName.split('.')[0]] = {};
            }
            filterJsonObject[keyName.split('.')[0]] = getDataData(filterJsonObject[keyName.split('.')[0]], `${keyName.substr(keyName.indexOf('.') + 1, keyName.length - 1)}`, keyVal);
        }
        return filterJsonObject;
    };

    let subFieldFindOptions = {};
    if (options && options.fields) {
        for (const count in options.fields) {
            if (options.fields && options.fields.hasOwnProperty(count)) {
                subFieldFindOptions = getDataData(subFieldFindOptions, count, options.fields[count]);
                if (count.indexOf('.') === -1) {
                    findOptions[count] = options.fields[count];
                } else {
                    if (count.split('.').length > 2) {
                        findOptions[`${count.split('.')[0]}.${count.split('.')[1]}`] = 1
                    } else {
                        findOptions[count] = options.fields[count]
                    }
                }
            }
        }
    }
    // consoleLog('findOptions ...... >>>' + JSON.stringify(findOptions))
    // consoleLog('subFieldFindOptions ...... >>>' + JSON.stringify(subFieldFindOptions))

    db.collection(collectionName).find(collFilter, findOptions).skip(querySkipCount).limit(queryLimit).sort(sort).toArray(function (err, result) {

        return getDottedFieldValue({
            db,
            result,
            options: {fields: subFieldFindOptions},
            DBDefination: dbList[collectionName]
        }).then(finalResult => {
            // consoleLog('finally ... ' + JSON.stringify(finalResult));
            // consoleLog("final appended result >> >> " ,result);
            d.resolve(finalResult)
        });

    });
    return d.promise;
};

export const mongoRemove = function (db, collectionName, filter) {
    const d = q.defer();
    db.collection(collectionName).remove(filter, function (err, result) {
        d.resolve(result);
    });
    return d.promise;
};
