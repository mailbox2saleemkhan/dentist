export const dbnames = {
    "Users": "Users",
    "Appointments": "Appointments",
};

export const departments = ["Teeth Whitening", "Teeth CLeaning", "Quality Brackets", "Modern Anesthetic"];
export const fieldTypes = {
    "String": "String",
    "Number": "Number",
    "Object": "Object",
    "fk": "fk",
    "Boolean": "Boolean",
    "Date": "Date"
};
export const mongoUrl = "mongodb://" + "127.0.0.1" + ":" + "27017" + "/" + "dentistApp";
export const tutorialLinks = [];

export const dbActions = {
    "INSERT": "INSERT",
    "UPDATE": "UPDATE",
    "DELETE": "DELETE",
};

//...................................common constants

export const roles = {
    "admin": "admin",
    "patient": "patient",
};

export const userStatus = {
    'active': 'Active',
    'inactive': 'InActive',
};
//...................................common constants ends


export const dbList = {
    [dbnames.Users]: {
        fields: [

            {
                name: "password",
                type: fieldTypes.String,
            },
            {
                name: "address",
                type: fieldTypes.String,
            },
            {
                name: "email_id",
                type: fieldTypes.String,
            },
            {
                name: "name",
                type: fieldTypes.String,
            },
            {
                name: "tokens",
                type: fieldTypes.String,
                multi: true
            },
            {
                name: "phone_no",
                type: fieldTypes.String,
            },
            {
                name: "role",
                type: fieldTypes.String,
                options: roles,
            },
            {
                name: "user_type",
                type: fieldTypes.String,
                options: userStatus,
                mandatory: true
            },

        ],
    },
    [dbnames.Appointments]: {
        fields: [

            {
                name: "name",
                type: fieldTypes.String,
            },
            {
                name: "email_id",
                type: fieldTypes.String,
            },
            {
                name: "phone_no",
                type: fieldTypes.String,
            },
            {
                name: "department",
                type: fieldTypes.String,
            },
            {
                name: "patient_id",
                type: fieldTypes.fk,
                referred_collection: dbnames.Users,
            },
            {
                name: "time",
                type: fieldTypes.String,
            },
            {
                name: "date",
                type: fieldTypes.String,
            },


        ],
    },

};