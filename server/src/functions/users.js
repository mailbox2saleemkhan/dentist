import q from 'q';
import {consoleLog } from '../Utility'

export const userBeforeJob = (db, document, options) => {
    var d = q.defer();
    consoleLog("inside user job with document ", document);
    consoleLog("inside user job with options ", options);
    d.resolve(document);
    return d.promise;
};
export const userAfterJob = (db, document) => {

};