import {dbnames} from '../constants'
import {userAfterJob, userBeforeJob} from './users'


export default {

    [dbnames.Users]: {
        beforeJob: userBeforeJob,
        afterjob: userAfterJob
    },

}