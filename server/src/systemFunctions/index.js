import {consoleLog} from "../Utility";
import {getData, insertData} from "../mongodb/connectors";
import {dbnames, userStatus} from '../constants';
import i18n from 'i18n';

export const writeJsonResponse = (req, res, result) => {
    consoleLog("result... ", result);
    setTimeout(_ => {

        const responseHead = {
            "Content-Type": "application/json"
        };
        if (result instanceof Error) {
            const responseToWrite = {
                status: "error",
                code: 500,
                message: result.message
            };
            res.writeHead(500, responseHead);
            res.write(JSON.stringify(responseToWrite));
            res.end();
        } else {
            const responseToWrite = {
                code: 200,
                response: result,
                status: "ok"
            };
            res.writeHead(200, responseHead);
            res.write(JSON.stringify(responseToWrite));
            res.end();
        }
    }, 0)

};


