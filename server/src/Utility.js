import mongo from 'mongodb';
import q from 'q';
import {dbActions, dbList, tutorialLinks, fieldTypes} from './constants';
import {mongoQuery} from './mongodb/curd_operations';
import i18n from 'i18n';

const objectID = mongo.ObjectID;

export const consoleLog = (param1, param2) => {
    const getFinalString = (param) => {
        if (typeof param === "object") {
            return JSON.stringify(param)
        }
        return param
    };
    let consoleString = getFinalString(param1);
    if (param2) {
        consoleString += getFinalString(param2)
    }
    console.log(consoleString);
};

export const getDeliveryAmountBasedOnSlabs = ({amountSlabs, lat1, lon1, lat2, lon2}) => {
    // todo :  use amount slabs to calculate delivery amount
    return getStraightLineDistanceBetweenTwoPoints({lat1, lon1, lat2, lon2})
};

export const getStraightLineDistanceBetweenTwoPoints = ({lat1, lon1, lat2, lon2, unit = "K"}) => {
    // 'M' is statute miles (default)
    // 'K' is kilometers
    // 'N' is nautical miles
    //
    // consoleLog(lat1)
    // consoleLog(lat2)
    // consoleLog(lon1)
    // consoleLog(lon2)
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1 / 180;
        var radlat2 = Math.PI * lat2 / 180;
        var theta = lon1 - lon2;
        var radtheta = Math.PI * theta / 180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344
        }
        if (unit == "N") {
            dist = dist * 0.8684
        }
        // consoleLog(dist)
        return dist;
    }
};

export const getToken = _ => require('crypto').createHash('sha1').update(objectID().toString()).digest("hex");

const getFieldVal = (options, document, fieldData) => {
    // consoleLog('options', options)
    // consoleLog('doc', document)
    // consoleLog('fieldData ', fieldData)
    let fieldVal = undefined;
    if (options && options.operation && options.operation === dbActions.INSERT) {
        if (document && document.hasOwnProperty(fieldData.name)) {
            if (fieldData.type && fieldData.type === fieldTypes.fk) {
                if (document[fieldData.name]._id) {
                    fieldVal = document[fieldData.name]._id;
                }
            } else {
                fieldVal = document[fieldData.name];
            }
        }
    } else if (options && options.oldDocument && options.oldDocument.hasOwnProperty(fieldData.name)) {
        if (fieldData.type && fieldData.type === fieldTypes.fk) {
            if (options.oldDocument[fieldData.name]._id) {
                fieldVal = options.oldDocument[fieldData.name]._id;
            }
        } else {
            fieldVal = options.oldDocument[fieldData.name];
        }
    }
    return fieldVal;
};

export const checkForMandatoryFieldsAndValidFkValues = (db, options, document, dbName) => {
    // consoleLog('inside checkForMandatoryFieldsAndValidFkValues')
    const d = q.defer();
    const colFkFields = dbList[dbName].fields.filter(function (fieldData) {
        return fieldData.type && fieldData.type === fieldTypes.fk;
    });
    const colMandatoryFields = dbList[dbName].fields.filter(function (fieldData) {
        return fieldData.mandatory ? fieldData.mandatory : false;
    });

    // consoleLog('colMandatoryFields', colMandatoryFields)
    // consoleLog('colFkFields', colFkFields)
    iterator(colFkFields, function (index, eachResult) {
        const fieldVal = getFieldVal(options, document, eachResult);
        if (fieldVal !== undefined) {
            return mongoQuery(db, eachResult.referred_collection, {"_id": fieldVal}).then(function (recordData) {
                if (recordData && recordData.length === 0) {
                    d.reject(new Error(eachResult.name + ("fkNotMatched")));
                }
            })
        }
    }).then(_ => {
        if (colMandatoryFields && colMandatoryFields.length > 0) {
            for (const eachFieldData in colMandatoryFields) {
                if (colMandatoryFields.hasOwnProperty(eachFieldData)) {
                    if (getFieldVal(options, document, colMandatoryFields[eachFieldData]) === undefined) {
                        consoleLog('got error', colMandatoryFields[eachFieldData].name + ("mandatoryFields"))
                        d.reject(new Error(colMandatoryFields[eachFieldData].name + ("mandatoryFields")));
                        return;
                    }
                }
            }
        }
    }).then(_ => {
        d.resolve(document)
    });

    return d.promise;
};

export const getEncPassword = password => require("crypto").createHash('sha256').update(password).digest("hex");


export const iterator = (array, task) => {
    return new Promise(function (resolve, reject) {
        const length = array ? array.length : 0;
        if (length === 0) {
            resolve();
            return;
        }
        const index = 0;
        const loop = function (index) {
            try {
                const onResolve = function () {
                    index = index + 1;
                    if (index === array.length) {
                        resolve();
                    } else {
                        loop(index);
                    }
                };
                try {
                    const p = task(index, array[index]);
                    if (!p) {
                        onResolve();
                        return;
                    }
                    p.then(onResolve)
                        .catch(function (err) {
                            reject(err)
                        })
                } catch (e) {
                    reject(e)
                }
            } catch (e) {
                reject(e)
            }
        };
        loop(index);
    })
};

export const resolveQueryFilters = (args) => {
    let finalColFilter = {};
    if (args && args.filter && Object.keys(args.filter).length > 0) {
        for (const filterKey in args.filter) {
            if (args.filter.hasOwnProperty(filterKey)) {
                finalColFilter[filterKey] = resolveIdsForFilter(filterKey, args.filter[filterKey]);
            }
        }
    }
    consoleLog("finalColFilter ", finalColFilter)
    return finalColFilter;
};
export const resolveIdsForFilter = (filterKey, filterKeyVal) => {

    let finalColFilter = undefined;
    if (filterKey.toString() === "_id") {
        finalColFilter = getIdResolved(filterKeyVal);
    } else if (filterKey.toString().indexOf(".") !== -1 && filterKey.split(".")[1] === "_id") {
        // dotted fields case
        finalColFilter = getIdResolved(filterKeyVal);
    } else if (typeof filterKeyVal === 'object') {
        if (Array.isArray(filterKeyVal)) {
            finalColFilter = [];
            for (const arrayFilterSubKey in filterKeyVal) {
                if (filterKeyVal.hasOwnProperty(arrayFilterSubKey)) {
                    const subFieldVal = filterKeyVal[arrayFilterSubKey];
                    if (typeof subFieldVal === 'object' && Object.keys(subFieldVal).length > 0) {
                        let finalJson = {};
                        for (const subFilterKey in subFieldVal) {
                            if (subFieldVal.hasOwnProperty(subFilterKey)) {
                                // consoleLog("subFilterKey .."+JSON.stringify(subFilterKey))
                                // consoleLog("subFieldVal[subFilterKey] .."+JSON.stringify(subFieldVal[subFilterKey]))
                                finalJson[subFilterKey] = resolveIdsForFilter(subFilterKey, subFieldVal[subFilterKey]);
                            }
                        }
                        finalColFilter.push(finalJson);
                        finalJson = {};
                    } else {
                        finalColFilter.push(subFieldVal);
                    }
                }
            }
        } else if (Object.keys(filterKeyVal).length > 0) {
            finalColFilter = {};
            for (const objFilterSubKey in filterKeyVal) {
                if (filterKeyVal.hasOwnProperty(objFilterSubKey)) {
                    finalColFilter[objFilterSubKey] = resolveIdsForFilter(objFilterSubKey, filterKeyVal[objFilterSubKey]);
                }
            }
        }
    } else {
        finalColFilter = filterKeyVal;
    }
    return finalColFilter;
};

export const convertFieldValsBasedOnRespectiveTypes = (updateInsertJson, DBDefination) => {
    // consoleLog("updateInsertJson" + JSON.stringify(updateInsertJson))
    // consoleLog("DBDefination" + JSON.stringify(DBDefination))
    if (DBDefination && DBDefination.fields && DBDefination.fields.length > 0) {

        for (const fieldVal in DBDefination.fields) {

            if (DBDefination.fields.hasOwnProperty(fieldVal) && DBDefination.fields[fieldVal].name && updateInsertJson && updateInsertJson.hasOwnProperty(DBDefination.fields[fieldVal].name)) {

                if (DBDefination.fields[fieldVal].type && DBDefination.fields[fieldVal].type !== fieldTypes.Object) {

                    if (DBDefination.fields[fieldVal].type === fieldTypes.fk && updateInsertJson[DBDefination.fields[fieldVal].name]._id) {
                        updateInsertJson[DBDefination.fields[fieldVal].name]._id = mongo.ObjectID(updateInsertJson[DBDefination.fields[fieldVal].name]._id)
                    }
                    else if (DBDefination.fields[fieldVal].type === fieldTypes.Number) {
                        updateInsertJson[DBDefination.fields[fieldVal].name] = Number(updateInsertJson[DBDefination.fields[fieldVal].name])
                    }
                }
                else if (DBDefination.fields[fieldVal].fields && DBDefination.fields[fieldVal].fields.length > 0) {

                    if (Array.isArray(updateInsertJson[DBDefination.fields[fieldVal].name])) {
                        for (const rowCount in updateInsertJson[DBDefination.fields[fieldVal].name]) {
                            if (updateInsertJson[DBDefination.fields[fieldVal].name].hasOwnProperty(rowCount)) {
                                const childDBFieldDefination = DBDefination.fields[fieldVal];
                                const childUpdateInsertJson = updateInsertJson[DBDefination.fields[fieldVal].name][rowCount];
                                convertFieldValsBasedOnRespectiveTypes(childUpdateInsertJson, childDBFieldDefination);
                            }
                        }
                    } else {
                        const childDBFieldDefination = DBDefination.fields[fieldVal];
                        const childUpdateInsertJson = updateInsertJson[DBDefination.fields[fieldVal].name];
                        convertFieldValsBasedOnRespectiveTypes(childUpdateInsertJson, childDBFieldDefination);
                    }

                }
            }
        }
    }
    return updateInsertJson;
};

export const getIdResolved = val => {

    const getArrayValResolved = (arrayObject) => {
        for (const arrayVal in arrayObject) {
            if (arrayObject.hasOwnProperty(arrayVal)) {
                arrayObject[arrayVal] = mongo.ObjectID(arrayObject[arrayVal])
            }
        }
        return arrayObject;
    };

    if (typeof(val) !== "object") {
        return mongo.ObjectID(val)
    } else {
        let finalVal = {};
        for (const objKey in val) {

            if (objKey === "$ne") {

                // $ne has been supported by this logic

                finalVal[objKey] = mongo.ObjectID(val[objKey]);
            }
            else if (objKey === "$in" || objKey === "$nin") {

                // $in && $nin has been supported by this logic

                if (!finalVal.hasOwnProperty(objKey)) {
                    finalVal[objKey] = [];
                }
                finalVal[objKey] = getArrayValResolved(val[objKey]);
            }
            else {
                finalVal[objKey] = val[objKey];
            }
        }
        return finalVal;
    }
};
