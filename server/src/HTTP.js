import {getData, updateData, insertData, removeData} from './mongodb/connectors';
import {
    dbnames,
    userStatus, departments
} from './constants';

import {consoleLog, getToken} from "./Utility";
import {writeJsonResponse} from "./systemFunctions";

export default (app) => {


    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        next();
    });

    app.all("/signup", (req, res) => {
        let userDetails = req.param("user");
        if (userDetails) {
            userDetails = JSON.parse(userDetails);
        }
        let insertedUserDetails = undefined;

        if (userDetails && userDetails.email_id && userDetails.password) {
            return getData({
                collection: dbnames.Users,
                filter: {email_id: userDetails.email_id},
            }).then(response => {
                if (response && response.length > 0 && response[0]._id) {
                    writeJsonResponse(req, res, new Error(("User already exists")));
                } else {
                    insertedUserDetails = {
                        password: userDetails.password,
                        user_type: userStatus.active,
                        phone_no: userDetails.phone_no,
                        name: userDetails.name,
                        email_id: userDetails.email_id,
                        tokens: [getToken()],
                        role: "patient",
                    };
                    return insertData({
                        collection: dbnames.Users,
                        insert: insertedUserDetails,
                    })
                }

            }).then(insertedRes => {
                if (insertedRes) {
                    writeJsonResponse(req, res, [{...insertedUserDetails, token: insertedUserDetails.tokens[0]}]);
                }
            }).fail(err => {

                writeJsonResponse(req, res, new Error(err));
            });
        } else {
            writeJsonResponse(req, res, new Error(("nameMailIdMandatoryErrorWhileSignup")));
        }
    });

    app.all("/get_departments", (req, res) => {
        writeJsonResponse(req, res, departments);
    });

    app.all("/login", (req, res) => {
        let userDetails = req.param("user");
        let userData = undefined;
        if (userDetails) {
            userDetails = JSON.parse(userDetails);
        }
        if (userDetails && userDetails.password && userDetails.email_id) {
            return getData({
                collection: dbnames.Users,
                filter: {"password": userDetails.password, email_id: userDetails.email_id},
                fields: {
                    "_id": 1,
                    "role": 1,
                    "name": 1,
                    "email_id": 1,
                    "phone_no": 1,
                    "user_type": 1,
                },
            }).then(response => {
                userData = response;
                if (userData && userData.length > 0 && userData[0].user_type && userData[0].user_type === userStatus.active) {
                    const token = getToken();
                    userData[0]["token"] = token;
                    return updateData({
                        collection: dbnames.Users,
                        filter: {_id: userData[0]._id.toString()},
                        update: {"$set": userDetails, "$push": {tokens: token}},
                    })
                } else {
                    throw new Error(('incorrectUserVerificationDetails'));
                }
            }).then(_ => {
                writeJsonResponse(req, res, [userData[0]]);
            }).fail(err => {
                writeJsonResponse(req, res, new Error(err));
            });
        } else {
            writeJsonResponse(req, res, new Error(("userInfoMissingWhileLogin")));
        }
    });

    app.all("/logout", (req, res) => {
        const token = req.param("token");
        const deviceToken = req.param("deviceToken");
        if (token) {
            return getData({
                collection: dbnames.Users,
                filter: {"tokens": {"$in": [token]}},
            }).then(userData => {
                // consoleLog("users data >> >> " ,userData);
                if (userData && userData.length > 0) {
                    return updateData({
                        collection: dbnames.Users,
                        filter: {_id: userData[0]._id.toString()},
                        update: {"$pull": {tokens: token, device_tokens: deviceToken}},
                    })
                } else {
                    //will give success result as no need to show error in logout case
                    return {"result": ("userTokenValidationError")};
                }
            }).then(userUpdatedRes => {
                writeJsonResponse(req, res, userUpdatedRes);
            }).fail(err => {
                writeJsonResponse(req, res, new Error(err));
            });
        } else {
            writeJsonResponse(req, res, {"result": ("userTokenValidationError")});
        }
    });

    app.use((req, res, next) => {
        const token = req.param("token");
        const authenticateUser = req.param("authenticateUser") ? req.param("authenticateUser") : true;
        if (authenticateUser && authenticateUser === true) {
            if (token) {
                return getData({
                    collection: dbnames.Users,
                    filter: {tokens: token}
                }).then(response => {
                    if (response && response.length > 0) {
                        next();
                    } else {
                        throw new Error(("userTokenValidationError"));
                    }
                }).fail(err => {
                    writeJsonResponse(req, res, new Error(err))
                })
            } else {
                writeJsonResponse(req, res, new Error(("userTokenValidationError")))
            }
        } else {
            next();
        }
    });


    app.all("/insert", (req, res) => {
        let requestQuery = req.param("insert");
        if (requestQuery) {
            requestQuery = JSON.parse(requestQuery);
        }
        const collection = requestQuery.collection;
        const insertJson = requestQuery.insert;
        if (collection && insertJson) {
            return insertData({collection: collection, insert: insertJson}).then(response => {
                consoleLog("insert res >> >> ", response);
                if (response && response.ops && response.ops.length > 0) {
                    writeJsonResponse(req, res, response.ops[0])
                } else {
                    writeJsonResponse(req, res, {})
                }
            }).fail(err => {
                writeJsonResponse(req, res, new Error(err))
            })
        } else {
            writeJsonResponse(req, res, new Error(('collectionAndInsertJsonMandatoryWithInsertUpdateCase')))
        }
    });

    app.all("/delete", (req, res) => {
        let requestQuery = req.param("query");
        if (requestQuery) {
            requestQuery = JSON.parse(requestQuery);
        }
        consoleLog("requestQuery >> >> ", requestQuery);
        const collection = requestQuery.collection;
        const filterJson = requestQuery.filter;
        if (collection && filterJson) {
            let finalgetCallJson = {collection: collection};
            if (filterJson) {
                finalgetCallJson["filter"] = filterJson
            }
            return removeData(finalgetCallJson).then(response => {
                consoleLog("remove res >> >> ", response);
                writeJsonResponse(req, res, response)
            }).fail(err => {
                consoleLog("remove fail res >> >> ", err.stack);
                writeJsonResponse(req, res, new Error(err.stack))
            })
        } else {
            writeJsonResponse(req, res, new Error(('collectionAndFilterMandatoryWithDeleteCase')))
        }
    });

    app.all("/query", (req, res) => {
        let requestQuery = req.param("query");
        if (requestQuery) {
            requestQuery = JSON.parse(requestQuery);
        }
        const collection = requestQuery.collection;
        const filterJson = requestQuery.filter;
        const fieldJson = requestQuery.fields;
        if (collection) {
            return getData({
                collection: collection,
                filter: (filterJson ? filterJson : {}),
                fields: (fieldJson ? fieldJson : {}),
                skip: requestQuery.skip ? requestQuery.skip : 0,
                limit: requestQuery.limit ? requestQuery.limit : 1000,
                sort: requestQuery.sort ? requestQuery.sort : {},
            }).then(response => {
                consoleLog("find res >> >> ", response);
                writeJsonResponse(req, res, response)
            }).fail(err => {
                consoleLog("find err >> >> ", err);
                writeJsonResponse(req, res, new Error(err))
            })
        } else {
            writeJsonResponse(req, res, new Error(('collectionMandatoryWithQueryCase')))
        }
    });

    app.all("/update", (req, res) => {
        let requestQuery = req.param("update");
        if (requestQuery) {
            requestQuery = JSON.parse(requestQuery);
        }
        const collection = requestQuery.collection;
        const updateJson = requestQuery.update;
        const filterJson = requestQuery.filter;
        if (collection && filterJson && updateJson) {
            return updateData({
                collection: collection,
                filter: filterJson,
                update: updateJson
            }).then(response => {
                consoleLog("update res >> >> ", response);
                writeJsonResponse(req, res, response)
            }).fail(err => {
                writeJsonResponse(req, res, new Error(err))
            })
        } else {
            writeJsonResponse(req, res, new Error(("collectionAndInsertJsonMandatoryWithInsertUpdateCase")))
        }
    });


};
